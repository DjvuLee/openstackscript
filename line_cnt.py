import subprocess
import os



def main():
    print("This will output the line number of the file in one dir")
    files = os.listdir("relation/")

    for file in files:
        file = os.path.join('relation/', file)
        command = ["wc", "-l", file]
        subprocess.call(command)

if __name__ == '__main__':
    main()
