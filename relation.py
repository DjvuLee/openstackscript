#!/usr/bin/python
'''
File: pingcluster.py
Author: DjvuLee
License: Apache V2
Description: this script is used to let all the machine in the cluster to ping a website, which will
left a record in the switch
'''


import threading
import paramiko
import sys
import os
import time


def keys(x):
    """docstring for fname"""
    return int(x.split("\t")[0].split("/")[-1])

def main():

    dir_name = "final"
    host_mac_file ="mac_host.txt"
    files = os.listdir(dir_name)


    mac_infos = {}
    with open(host_mac_file, "r") as fr:
        all_macs = fr.read().splitlines()
        for line in all_macs:
            tmp = line.split("\t")
            host_name = tmp[0]
            eths = tmp[1:]
            for eth_mac in eths:
                eth = eth_mac.split()[0].strip()
                mac = eth_mac.split()[1].strip()
                mac_infos[mac] = [host_name, eth]





    for file in files:
        relation = []
        real_file = os.path.join(dir_name, file)
        with open(real_file, "r") as fr:
            lines = fr.read().splitlines()
            for line in lines:
                port = line.split("\t")[1].strip()
                mac = line.split("\t")[0].strip().upper()

                if mac in mac_infos.keys():
                    item = port + "\t" + " ".join(mac_infos[mac]) + "\t" + mac + "\n"
                    relation.append(item)
                else:
                    print(port + "\t" + mac + "\t" + file)



        save_file = "relation/%s" %file
        relation = sorted(relation, key=lambda x: keys(x))
        with open(save_file, "w") as fr:
            fr.writelines(relation)

    print("finished process")





if __name__ == '__main__':
    main()
