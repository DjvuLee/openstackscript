# Author: DjvuLee
# Email: lihu723@gmail.com
# Licence: Apache V2

整个工程是用来得到每个物理机器的网卡和交换机的端口的连接关系。其中的脚本说明：

1. mac_host.py:  用来得到所有的mac地址

2. ping_cluster.py: 让所有的物理机器执行ping指令，以便让交换机ethnet-switching table里面得到mac地址和port的对应关系

3. switch.py: 让交换机运行show ethnet-switching table命令，并把结果送回过来

4. post_process.py: 处理交换机送回来的结果

5. relation.py: 匹配post_process.py处理的结果，并把它和mac_host.py得到的结果进行整合，得到最终的物理机器的网卡和交换机端口的对应关系。每个交换机的结果以一个文件表示。

6. line_cnt.py: 统计某个目录下的每个文件的行数，按照预期，每个文件的行数不应该少于30行，如果有某些少于30行，那么证明这个交换机上的连接有问题。

说明:  你按照上述的步骤来运行脚本即可。其中mac_post.py只需运行一次，因为交换机的table隔一段时间就会更新，后面的脚本你可能需要运行多次。
