#!/usr/bin/python
'''
File: pingcluster.py
Author: DjvuLee
License: Apache V2
Description: this script is used to let all the machine in the cluster to ping a website, which will
left a record in the switch
'''


import threading
import paramiko


username = "root"
password = "pica8"

cnt = 0
def process(ip):
    print(ip)
    command = '/pica/bin/pica_sh -c "show ethernet-switching table"'


    ssh=paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(ip, 22, username, password)
    stdin,stdout,stderr=ssh.exec_command(command)

    out = stdout.read().strip()

    ssh.close()
    save_result(out, ip)


def save_result(result, ip):
    """docstring for save_result"""

    file_name = "result/%s_swith.txt" %ip
    with open(file_name, "w") as fr:
        fr.writelines(result)

    print("finished %s" %ip)


def main():
    switch_ips = range(1, 25)
    for i in switch_ips:
        ip = "10.0.3.%d" %i
        t = threading.Thread(target=process, args=(ip, ))
        t.start()




if __name__ == '__main__':
    main()
