#!/usr/bin/python
'''
File: pingcluster.py
Author: DjvuLee
License: Apache V2
Description: this script is used to let all the machine in the cluster to ping a website, which will
left a record in the switch
'''


import threading
import paramiko
import sys
import time


username = "root"
err_host = []

def process(ip, eths):
    print(ip)

    ssh=paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    try:
        if ip == "10.1.0.1" or ip == "10.1.0.5":
            password = "root"
        else:
            password = "root123"
        ssh.connect(ip, 22, username, password)
    except Exception, e:
        print("%s: %s" %(ip, e))
        ssh.close()
        return

    for eth in eths:
        command = "ping -I %s -c 3 8.8.8.8" %eth
        #command = "ifconfig eth3 up"
        stdin,stdout,stderr=ssh.exec_command(command)

    ssh.close()



def main():
    args = sys.argv

    if len(args) < 2:
        print("you should give a network interface, such as eth0 or eth1")
        sys.exit(0)

    eths = args[1:]
    ips = [1, 5]
    ips.extend(range(12, 181))
    ips.extend(range(203, 209))


    ts = []
    for i in ips:
        ip = "10.1.0.%d" %i
        t = threading.Thread(target=process, args=(ip, eths))
        t.start()
        ts.append(t)

    for t in ts:
        t.join()

    print("finished\n")
    for item in err_host:
        print(item)



if __name__ == '__main__':
    main()
