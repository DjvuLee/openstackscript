#!/usr/bin/python
'''
File: pingcluster.py
Author: DjvuLee
License: Apache V2
Description: this script is used to get all the machine's net interface with the mac addr
'''


import threading
import paramiko
import sys
import time


username = "root"

final_result = []

def process(ip):
    print(ip)

    ssh=paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    try:
        if ip == "10.1.0.1" or ip == "10.1.0.5":
            password = "root"
        else:
            password = "root123"
        ssh.connect(ip, 22, username, password)
    except Exception, e:
        print("%s: %s" %(ip, e))
        ssh.close()
        return

    command = 'ifconfig | grep "eth" | tr -s " " | cut -d " " -f 1,5'
    stdin,stdout,stderr=ssh.exec_command(command)
    out = stdout.read().strip()

    net_info = ip + "\t" + out.replace("\n", "\t") + "\n"
    final_result.append(net_info)

    ssh.close()



def keys(val):
    return int(val.split('\t')[0].split(".")[3])


def main():
    ips = [1, 5]
    ips.extend(range(12, 181))
    ips.extend(range(203, 209))


    save_file = "mac_host.txt"

    ts = []
    for i in ips:
        ip = "10.1.0.%d" %i
        t = threading.Thread(target=process, args=(ip,))
        t.start()
        ts.append(t)

    for t in ts:
        t.join()

    global final_result
    final_result = sorted(final_result, key=(lambda x : keys(x)))
    with open(save_file, "w") as fr:
        fr.writelines(final_result)

    print("finished, and your result is save to the %s\n" % save_file)


if __name__ == '__main__':
    main()
