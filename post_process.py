#!/usr/bin/python

import threading
import sys
import os

def process(dir_name, name):
    """docstring for """

    result = []
    file_name = os.path.join(dir_name, name)
    with open(file_name, "r") as fr:
        lines = fr.read().splitlines()
        for line in lines:
            tmp = line.split()
            if len(tmp)< 6:
                continue
            switch_port = tmp[4].strip()
            if "ge-" in switch_port:
                result.append(tmp[1].strip() + "\t" + switch_port+"\n")

    save_result(result, name)


def save_result(result, name):
    file_name = os.path.join("final", name)

    with open(file_name, "w") as fr:
        fr.writelines(result)

    print("finished %s" %name)

def main():
    args = sys.argv

    if len(args) != 2:
        print("please input the file dir")
        sys.exit(1)

    dir_name = args[1]

    files = os.listdir(dir_name)


    for file in files:
        t = threading.Thread(target=process, args=(dir_name, file))
        t.start()




if __name__ == '__main__':
    main()
